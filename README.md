[![DOI](https://zenodo.org/badge/300028000.svg)](https://zenodo.org/badge/latestdoi/300028000)

Paper (open access): [General-Purpose Coarse-Grained Toughened Thermoset Model for 44DDS/DGEBA/PES ](https://doi.org/10.3390/polym12112547)

Data (CC-BY-SA): [link](https://drive.google.com/drive/folders/12gMdFHTKY72EbIEH1FxxG6dT5cf-i-ac?usp=sharing) 

* [Figure 3](tg-compare/tg-compare.ipynb)
* [Figure 4](system-size-dependence/system-size-dependence.ipynb)
* [Figure 5](gel-point/gel-point.ipynb)
* [Figure 6a](diffusivity-tg/diffusivity-tg.ipynb)
* [Figure 6b](diffusivity-tg/diffusivity-tg.ipynb)
* [Figure 7](system-size-dependence/3D-sq-time.ipynb)
* [Figure 8](rxn-fits-fo-safo/rxn-fits-safo.ipynb)
* [Figure 9a](sensitivity-study/t1/t1.ipynb)
* [Figure 9b](sensitivity-study/t1/t1.ipynb)
* [Figure 10a](sensitivity-study/t2/t2.ipynb)
* [Figure 10b](sensitivity-study/t2/t2.ipynb)
* [Figure 11](sensitivity-study/t2/t2.ipynb)

